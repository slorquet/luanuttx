############################################################################
# apps/ interpreters/lua/Makefile
#
#   Copyright (C) 2015, 2016 Gregory Nutt. All rights reserved.
#   Authors: Gregory Nutt <gnutt@nuttx.org>
#            Sebastien Lorquet <sebastien@lorquet.fr>
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
#
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in
#    the documentation and/or other materials provided with the
#    distribution.
# 3. Neither the name NuttX nor the names of its contributors may be
#    used to endorse or promote products derived from this software
#    without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
# FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
# COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
# INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
# BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
# OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
# AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
# LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
# ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
#
###########################################################################

-include $(TOPDIR)/Make.defs

# Set up build configuration and environment

CONFIG_INTERPRETERS_LUA_VERSION ?= "5.3.3"

LUA_VERSION = $(patsubst "%",%,$(strip $(CONFIG_INTERPRETERS_LUA_VERSION)))
LUA_PACKNAME = lua-$(LUA_VERSION).tar.gz
LUA_UNPACKNAME = lua-$(LUA_VERSION)
LUA_SRCDIR = $(LUA_UNPACKNAME)/src

INC =  -I. -I$(LUA_SRCDIR)

CFLAGS +=$(INC) -Dstrcoll=strcmp

# Built-in application info

CONFIG_INTERPRETERS_LUA_APPNAME ?= lua
CONFIG_INTERPRETERS_LUA_STACKSIZE ?= 2048
CONFIG_INTERPRETERS_LUA_PRIORITY ?= SCHED_PRIORITY_DEFAULT

APPNAME = $(CONFIG_INTERPRETERS_LUA_APPNAME)
STACKSIZE = $(CONFIG_INTERPRETERS_LUA_STACKSIZE)
PRIORITY = $(CONFIG_INTERPRETERS_LUA_PRIORITY)

CONFIG_INTERPRETERS_LUA_PROGNAME ?= lua$(EXEEXT)
PROGNAME = $(CONFIG_INTERPRETERS_LUA_PROGNAME)

# Files

CORE_O=	lapi.c lcode.c lctype.c ldebug.c ldo.c ldump.c lfunc.c lgc.c llex.c \
	lmem.c lobject.c lopcodes.c lparser.c lstate.c lstring.c ltable.c \
	ltm.c lundump.c lvm.c lzio.c
LIB_O=	lauxlib.c lbaselib.c lbitlib.c lcorolib.c ldblib.c liolib.c loslib.c \
	lmathlib.c lstrlib.c ltablib.c lutf8lib.c loadlib.c linit.c

ASRCS =
CSRCS = $(addprefix $(LUA_SRCDIR)/,$(CORE_O)) $(addprefix $(LUA_SRCDIR)/,$(LIB_O))
MAINSRC = lua_main.c

include $(APPDIR)/Application.mk
