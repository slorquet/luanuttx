# README #

This is a fork of lua 5.3.3 for nuttx.

Installation: Just clone the git repo in the apps/interpreter subdirectory, then enable the app in menuconfig. system/readline must also be enabled.

Restrictions: 

 * io does not have tmpfile,setvbuf 
 * os does not have execute,setlocale
